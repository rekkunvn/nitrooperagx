const express = require('express')
const { v4: uuidv4 } = require('uuid')
const request = require('request')
const app = express()
const port = 3000

app.get('/', (req, res) => {
    let uuid = uuidv4()
    request.post({
        url: 'https://api.discord.gx.games/v1/direct-fulfillment',
        headers: {
            'Accept': '*/*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.8',
            'Cache-Control': 'no-cache',
            'Content-Length': 56,
            'Content-Type': 'application/json',
            'Dnt': 1,
            'Origin': 'https://www.opera.com',
            'Pragma': 'no-cache',
            'Referer': 'https://www.opera.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'cross-site',
            'Sec-Gpc': 1,
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36 OPR/65.0.3467.48',
            'Accept-Charset': 'utf-8',
        },
        body: {
            partnerUserId: uuid
        },
        json: true
    }, (err, resp, body) => {
        if(err) res.redirect('/')
        res.redirect(`https://discord.com/billing/partner-promotions/1180231712274387115/${body.token}`)
    })
})  

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})